# The Kameňák

Toto je repozitář ḱódu týmu *The Kameňák* na soutěži [Autobot 2017](http://autobot.sou-dubska.cz/). Je naprogramován za pomocí NXC4EV3,
jako jehož demonstrační příklad byl mimo jiné mířen.

## Programovací prostředky

* [NXC4EV3](http://www.robosoutez.cz/index.php?sekce=nxc4ev3&id=nxc4ev3)
* [http://www.graphviz.org/](Graphviz) (na grafy)
